#!/usr/bin/env python36
import sys
import datetime
import logging
import traceback
import time

import bittrex

from config import api_key, secret_key, loss_time, exceptions, min_lot_price,\
    min_profit, max_trade_pairs_count, min_order
from database import Database
from utils import get_time_delta_from_string, get_rank, get_coin_name


logging.basicConfig(filename="bot.log", level=logging.INFO)


bot = bittrex.Bittrex(api_key, secret_key)


# TODO dictionary result
db = Database('Bittrex')


class Coin:
    def __init__(self, name, balance=0):
        self.name = name
        self.balance = balance
        self.market_name = 'BTC-' + self.name

    @property
    def is_subject_to_sale(self):
        exceptions = self.check_exceptions()
        balance = self.check_balance()
        delta = self.check_delta()
        profit = self.check_profit()
        result = (exceptions and balance) and (delta or profit)
        return result

    def price(self, cost_type='Ask'):
        """
        Get price Ask or Bid depending on argument
        :return: price
        """
        shift = {
            'Ask': -0.00000001,
            'Bid': 0.00000001
        }
        if cost_type in shift:
            ticker = bot.get_ticker(self.market_name)
            if ticker['message'] != 'INVALID_MARKET':
                return ticker['result'][cost_type] + shift[cost_type]
            else:
                return 0

    def get_last_order(self, deal):
        last_orders = db.select(
            'orders', coin=self.name, type=deal, result='executed'
        )
        if not last_orders:
            result = {'ok': False, 'content': None}
        else:
            result = {
                'ok': True,
                'content': {
                    'Price': last_orders[-1][7],
                    'Quantity': last_orders[-1][6],
                    'timeDelta': get_time_delta_from_string(last_orders[-1][0])
                }
            }
        return result

    def check_delta(self):
        """
        If true sell coin
        :return:
        """
        last_order = self.get_last_order('BUY')
        if not last_order['ok']:
            return True
        return last_order['content']['timeDelta'] > loss_time

    def check_balance(self):
        """
        If coin balance greater than minimal lot price, sell is forbidden
        :return:
        """
        return self.balance > min_lot_price

    def check_exceptions(self):
        """
        if coin in exceptions coin not buy and not sell
        :return:
        """
        return self.name not in exceptions

    def check_profit(self):
        """
        if profit greater than minimal profit coin sell
        :return: True if profit > min_profit else False
        """
        last_order = self.get_last_order('BUY')
        if last_order['ok']:
            lop = last_order['content'].get('Price', 0)
            loq = last_order['content'].get('Quantity', 0)
            if not any([lop, loq]):
                return True
            profit = ((self.price() * loq) - (lop * loq)) / (lop * loq)
        else:
            profit = 0
        return profit > min_profit

    def open_order(self, type):
        if type == 'BUY':
            rate = self.price('Bid')
            quantity = min_lot_price / rate
            order = bot.buy_limit(self.market_name, quantity, rate)
        else:
            quantity = self.balance
            rate = self.price()
            order = bot.sell_limit(self.market_name, quantity, rate)
        if order['result']:
            uuid = order['result']['uuid']
            now = datetime.datetime.now().strftime('%Y.%m.%d.%H.%M.%S')
            db.insert(
                    'orders', closed=0, created=now, uuid=uuid,
                    coin=self.name, type=type, count=quantity, price=rate
                )
            logging.info(
                '{} order\topen\t{}\t{}\t{}\t{}'.format(
                        type, uuid, self.name, quantity, rate
                    )
                )

    def __str__(self):
        return '{}:{}'.format(self.name, self.balance)


# TODO make iterator
class CoinsList:
    @property
    def coins(self):
        return []

    def __str__(self):
        coins = ''
        for coin in self.coins:
            coins += '{}:{}\n'.format(coin.name, coin.balance)
        return coins


class Wallet(CoinsList):
    @property
    def coins(self):
        balance = bot.get_balances()['result']
        coins = list(
            filter(
                lambda x:
                    x['Available'] > 0 and
                    x['Currency'] not in exceptions,
                balance
                )
            )
        return [Coin(coin['Currency'], coin['Balance']) for coin in coins]


class Market(CoinsList):
    def __init__(self):
        self.wallet = Wallet()

    @property
    def coins(self):
        market_data = bot.get_market_summaries()['result']
        exception_market = [
            'BTC-' + coin.name for coin in self.wallet.coins
            if coin.balance > min_order
            ]
        market = list(filter(lambda x: all([
            'BTC' in x['MarketName'],
            not x['MarketName'] in exception_market
        ]), market_data))
        market.sort(key=get_rank, reverse=True)
        return [
            Coin(get_coin_name(coin['MarketName'])) for coin in market
            ]


class Trade:
    market = Market()
    wallet = Wallet()

    def cancel_open_orders(self):
        orders = db.select('orders', closed=0)
        for order in orders:
            response = bot.cancel(order[4])
            action = 'cancelled' if response['success'] else 'executed'
            db.update_by_id('orders', order[3], closed=1, result=action)
            logging.info(
                '{} order\t{}\t{}\t{}\t{}\t{}\t{}'.format(
                    order[1], action, order[4], order[2],
                    order[6], order[7], order[0]
                )
            )

    def open_sell_orders(self):
        for coin in self.wallet.coins:
            if coin.is_subject_to_sale:
                coin.open_order('SELL')

    def open_buy_order(self):
        btc = [
            coin.balance for coin in self.wallet.coins
            if coin.name == 'BTC'
            ][0]
        if (btc > min_lot_price and
                len(self.wallet.coins) < max_trade_pairs_count):
            coin = self.market.coins[0]
            coin.open_order('BUY')


if __name__ == '__main__':
    trade = Trade()
    try:
        logging.info(
            '### Start {} ###'.format(
                datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
                )
            )
        trade.cancel_open_orders()
        trade.open_sell_orders()
        time.sleep(5)
        trade.open_buy_order()
    except Exception:
        t = ''.join(traceback.format_exception(*sys.exc_info()))
        logging.error(t)
    finally:
        logging.info(
            '### End {} ###'.format(
                datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
                )
            )
