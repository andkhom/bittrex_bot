import datetime
import re


def get_additional_quotes(string):
    if type(string) in [int, float]:
        string = str(string)
    return '\'' + string + '\''


def get_time_delta_from_string(now_string):
    return datetime.datetime.now() - parse_string_to_date(now_string)


def parse_string_to_date(string):
    time_list = list(map(int, string.split('.')))
    return datetime.datetime(*time_list)


def get_rank(input_json):
    ask = input_json['Ask'] - 0.00000001
    bid = input_json['Bid'] + 0.00000001
    volume = input_json['Volume']
    rank = ((ask - bid)/bid)*volume
    return rank


def get_change_rank(input_json):
    try:
        prev_day = input_json['PrevDay']
        last = input_json['Last']
        rank = (last - prev_day) / prev_day
    except KeyError:
        rank = 0
    return rank


def get_high_low_rank(input_json):
    high = input_json['High']
    low = input_json['Low']
    rank = (high - low) / low
    return rank


def get_coin_name(market_name):
    pattern = '[-]'
    market_list = re.split(pattern, market_name)
    return market_list[-1]
