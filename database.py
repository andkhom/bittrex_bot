import sqlite3
from utils import get_additional_quotes as quotes


# TODO f-strings


class Database:
    '''
    mini ORM for sqlite3
    '''
    def __init__(self, db_name):
        self.conn = sqlite3.connect(db_name)
        self.cursor = self.conn.cursor()

    @property
    def tables(self):
        self.cursor.execute(
            'SELECT name FROM sqlite_master WHERE type=\'table\''
            )
        return self.cursor.fetchall()

    def table_columns(self, table):
        self.cursor.execute('PRAGMA table_info(orders)')
        return ', '.join([column[1] for column in self.cursor.fetchall()])

    def create_table(self, table, **kwargs):
        sql_list = ['{} {}'.format(kwarg, kwargs[kwarg]) for kwarg in kwargs]
        sql = 'CREATE TABLE {} ({});'.format(table, ','.join(sql_list))
        self.cursor.execute(sql)

    def add_column(self, table, **kwargs):
        sql_list = ['{} {}'.format(kwarg, kwargs[kwarg]) for kwarg in kwargs]
        sql = 'ALTER TABLE {} ADD COLUMN {}'.format(table, ','.join(sql_list))
        self.cursor.execute(sql)

    def drop_table(self, name):
        self.cursor.execute('DROP TABLE {}'.format(name))

    def insert(self, table, **kwargs):
        fields = [quotes(kwarg) for kwarg in kwargs]
        values = [quotes(kwargs[kwarg]) for kwarg in kwargs]
        sql = 'INSERT INTO {} ({}) VALUES ({})'.format(
            table, ', '.join(fields), ', '.join(values))
        self.cursor.execute(sql)
        self.conn.commit()

    def select(self, name, query='*', **kwargs):
        if kwargs:
            values = ['{} = {}'.format(
                kwarg, quotes(kwargs[kwarg])
                ) for kwarg in kwargs]
            sql = 'SELECT {} FROM {} WHERE {}'.format(
                query, name, ' AND '.join(values)
                )
        else:
            sql = 'SELECT {} FROM {}'.format(query, name)
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def update_by_id(self, table, id, **kwargs):
        values = ['{} = {}'.format(
            kwarg, quotes(kwargs[kwarg])
            ) for kwarg in kwargs]
        sql = 'UPDATE {} SET {} WHERE id = {}'.format(
            table, ','.join(values), id)
        self.cursor.execute(sql)
        self.conn.commit()


if __name__ == '__main__':
    db = Database('Bittrex')
