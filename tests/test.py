import sys
import os
import unittest
sys.path.append(os.getcwd())
from bot import *


class TestCoin(unittest.TestCase):
    wallet = Wallet()
    market = Market()

    def test_ask_bid_returns_float(self):
        for coin in self.market.coins[:10]:
            self.assertIsInstance(coin.ask, float)
            self.assertIsInstance(coin.bid, float)

    def test_last_order_return_dict(self):
        for coin in self.market.coins[:10]:
            last_order_buy = coin.get_last_order('BUY')
            self.assertTrue('ok' in last_order_buy and 'content' in last_order_buy)
            last_order_sell = coin.get_last_order('SELL')
            self.assertTrue('ok' in last_order_sell and 'content' in last_order_sell)

    def test_check_delta(self):
        xrp = Coin('XRP')
        self.assertTrue(xrp.check_delta())
        wax = Coin('WAX')
        self.assertFalse(wax.check_delta())


if __name__ == '__main__':
    unittest.main()
